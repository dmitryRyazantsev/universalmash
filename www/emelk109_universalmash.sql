-- phpMyAdmin SQL Dump
-- version 3.5.8
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 03 2013 г., 14:19
-- Версия сервера: 5.0.91-community-log
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `emelk109_universalmash`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `title_en` varchar(255) NOT NULL default '',
  `meta_keywords` varchar(255) NOT NULL default '',
  `meta_description` varchar(255) NOT NULL default '',
  `text` text NOT NULL,
  `short_text` text NOT NULL,
  `img` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `title_en`, `meta_keywords`, `meta_description`, `text`, `short_text`, `img`) VALUES
(1, 'ООО “ВКФ “Универсалмаш”', 'about', 'тротуарная плитка, плитка, брусчатка, гиперпрессование,Металлоконструкции, детали для тепловозов, детали для сельхоз техники, з/ч для тепловозов, з/ч для сельхоз техники', 'Предприятие производит тротуарную плитку, Металлоконструкции,с/х технику и запасные части к ней, а также горно-шахтное оборудование и запасные части к нему, запасные части тепловозов, а также оборудование горно-металлургического производства. ', '<p class="lead history" >В 2001 году на базе ОАО «ХК «Лугансктепловоз» создан ДП «Машиностроитель» по производству почвообрабатывающей с/х техники. Начиная с 2004 г. и по настоящее время предприятие освоило и производит с/х оборудование, а также запасные части как для отечественной с/х техники так и для импортной. В 2012 г. на базе ДП «Машиностроитель» создано предприятие ООО «ВКФ «Универсалмаш».</p>\r\n            <p class="lead history">Предприятие производит с/х технику и запасные части к ней, а также горно-шахтное оборудование и запасные части к нему, запасные части тепловозов, а также оборудование горно-металлургического производства. Предприятие производит ремонтно-строительные работы на пром. площадках.</p>', '', ''),
(2, 'Контакты', 'contacts', 'Контакты', 'Контакты ООО “ВКФ” Универсалмаш”', '<address>\r\n              <strong>ООО “ВКФ” Универсалмаш”</strong><br/><br/>\r\n              Адресс: Украина, Луганск, ул.Фрунзе, 107<br/>\r\n              Факс: +38 (0642) 71-71-52</br>\r\n              Телефон: +38 (0642) 34-50-63</br>\r\n              +38 (050) 90-17-480 </br>\r\n              +38 (050) 17-20-045 </br>\r\n              +38 (050) 32-85-252 </br>\r\n              E-mail: <a href="mailto:universalmash@ukr.net">universalmash@ukr.net</a></br>\r\n             </address>', '', ''),
(3, 'З/Ч для тепловозов', 'parts', 'Ремонт колесных пар вагонов, Тяга тормозная, Ремонт колесных пар электровозов ОПЭ-1А, З/Ч для тепловозов, Детали для тепловозов, ремонт тепловозов, Коллектор водяной на тепловоз, Кожух редуктора, Корпус редуктора', 'Ремонт колесных пар вагонов, Тяга тормозная, Ремонт колесных пар электровозов ОПЭ-1А, З/Ч для тепловозов, Детали для тепловозов, ремонт тепловозов, Коллектор водяной на тепловоз, Кожух редуктора, Корпус редуктора', 'Широкий ассортимент запчастей для тепловозов, а также ремонтные работы.', 'Широкий ассортимент запчастей для тепловозов, а также ремонтные работы.', '/images/parts.png'),
(4, 'Металлоконструкции', 'metallokonstruktsiy', '', '', '<p>Квалифицированные специалисты ООО «ПКФ «Универсалмаш»  осуществляют проектирование и разработку металлоконструкций с помощью новейших компьютерных программ 3-мерного объемного моделирования, что позволяет значительно сократить сроки разработки документации, повысить качество исполнения, предложив потребителю надежные и одновременно функциональные решения для зданий и сооружений.\n</p>\n<p>Благодаря опыту и профессионализму наших специалистов  успешно осуществляется проектирование сложнейших строительных металлоконструкций, с учетом пожеланий заказчика, а также предпринимается индивидуальный подход к проектированию объектов различного уровня сложности.</p>\n\n<p>На производственных площадях изготавливаются сварные металлоконструкции различного уровня сложности - от типовых до эксклюзивных, с оптимальным соотношением: цена – качество + высокая степень надежности.\n</p>\n<p>Исходя из назначения и ответственности объекта строительства, мы имеет техническую возможность осуществлять контроль качества материалов и конструкций в соответствии с требованиями проекта заказчика.\n</p>\n<p>Наше предприятие имеет опыт изготовления каркасов  для промышленных сооружений, ангаров, складов, жилых зданий, спортивных комплексов, оборудования для конюшен и т.д.\n</p>\n<p>При наличии у заказчика проектной документации возможно изготовление металлоконструкций на заказ по представленным проектам. При их отсутствии возможна разработка проектной документации силами наших инженеров, в том числе и на металлоконструкции нестандартных видов.</p>', 'Производство металлоконструкций любой сложности <br/> до 40 тн.', 'images/mk.png'),
(5, 'Тротуарная плитка', 'plitka', 'тротуарная плитка, плитка, брусчатка, гиперпрессование', 'Тротуарная плитка повышенной прочности методом гиперпрессования.', '<p>ООО ”ВКФ “Универсалмаш”  один из лидеров по производству тротуарной плитки-брусчатка-монолит методом гиперпрессования. Представлена в сером и красном цвете.</p>\r\n            <p>Широко используется для мощения дорог, автомобильных стоянок, железнодорожных переездов, транспортных остановок, дворовых и внутриквартальных территорий.</p>\r\n            <p>Преимущества нашей продукции:</p>\r\n            <ul>\r\n              <li>Высокая прочность и долговечность;</li>\r\n              <li>Точные размеры (240х120х65);</li>\r\n              <li>Долговечный цветной окрас;</li>\r\n          </ul>\r\n <p>Также стоит отметить, что тротуарная плитка на предприятии производится при помощи высококлассного оборудования последнего поколения. Квалифицированный персонал, работающий на производстве, обладает большим опытом и знает абсолютно все нюансы изготовления качественной тротуарной плитки, что положительно влияет на качество всей продукции.</p>\r\n          <p>Мы готовы работать как с частными лицами, так и с предприятиями.</p>\r\n          <p>Принимаем как единичные так и оптовые заказы.</p>\r\n          <p>Постоянным клиентам представляются скидки.</p>\r\n \r\n          <table class="table table-hover">\r\n            <thead>\r\n              <tr>\r\n                <td>№</td>\r\n                <td>Название и размеры</td>\r\n                <td>Цвета</td>\r\n                <td>Цена</td>\r\n              </tr>\r\n            </thead>\r\n            <tr>\r\n              <td>1.</td>\r\n              <td class=''table_size''>240x120x65 <br/>\r\n                (повышенной прочности для мощения стоянок, заправок и ж/д транспортных переездов)</td>\r\n              <td>\r\n                серая<br/>\r\n                красная\r\n              </td>\r\n              <td>\r\n                120 грн. кв.м<br/>\r\n                135 грн. кв.м\r\n              </td>\r\n            </tr>\r\n            <tr>\r\n              <td>2.</td>\r\n              <td class=''table_size''>240x120x55 <br/>\r\n                (повышенной прочности для мощения заездов под легковые автомобили и тротуары)</td>\r\n              <td>\r\n                серая<br/>\r\n                красная\r\n              </td>\r\n              <td>\r\n                115 грн. кв.м<br/>\r\n                130 грн. кв.м\r\n              </td>\r\n            </tr>\r\n            <tr>\r\n              <td>3.</td>\r\n              <td class=''table_size''>Поребрик <br/>(500x200x60)</td>\r\n              <td>\r\n                серая<br/>\r\n                красная\r\n              </td>\r\n              <td>\r\n                35 грн. м/п<br/>\r\n                40 грн. м/п\r\n              </td>\r\n            </tr>\r\n            <tr>\r\n              <td>4.</td>\r\n              <td class=''table_size''>Отлив</td>\r\n              <td>\r\n                серая<br/>\r\n                красная\r\n              </td>\r\n              <td>\r\n                35 грн. м/п<br/>\r\n                40 грн. м/п\r\n              </td>\r\n            </tr>\r\n          </table>\r\n          <p class="snoska">* поддоны возвратная тара (под залог) или оплачиваются 25 грн. за 1 шт.</p>\r\n<br/>\r\n<h3>Метод гиперпрессованния</h3>\r\n\r\n<p>Основной метод полусухого гиперпрессованния применяется к цементно-известняковой смеси, в составе которого содержатся минеральные добавки. Этот способ изготовления тротуарной плитки заключается в сжатии исходного раствора, обладающего влажностью от 8% до 10%. Причем сила давления, прикладываемого в нашем случае не ниже, чем 25МПа.</p>\r\n\r\n <p>Стоит отметить, что в настоящее время гиперпрессованная тротуарная плитка считается лучшей среди аналогов. В частности, этот строительный материал обладает высокими свойствами прочности и морозостойкости, получающимися за счет практически полного отсутствия в готовой тротуарной плитке всевозможных пор и пустот.</p>\r\n \r\n <p>Для повышения прочности тротуарной плитки и ускорения самого процесса изготовления уже готовые изделия пропариваются в специальной камере, где установлен особый температурный режим. После окончания этого процесса тротуарная плитка в течение длительного времени хранится в специально оборудованном складском помещении.</p>\r\n\r\n <p>Чтобы тротуарная плитка обладала требующимся цветом, необходимо применение качественных светостойких пигментов. В нашей компании используются импортные качественные пигменты, придающие материалу стойкий оттенок, который практически не восприимчив к постоянному воздействию окружающей среды. Пигменты отличного качества позволяют нам изготавливать тротуарную плитку нескольких цветов.</p>\r\n <p>При изготовлении гиперпрессованной тротуарной плитки мы используем только проверенное сырье, которое нередко доставляется из других регионов Украины.</p>\r\n\r\n <p>В результате таких систематически проводимых исследований применяемых компонентов, мы можем гарантировать, что вся производимая тротуарная плитка абсолютно безопасна для здоровья человека и животных.</p>', 'Тротуарная плитка повышенной прочности методом гиперпрессования.', 'images/plitka.png'),
(6, 'Услуги', 'services', '', '', '<ol>\r\n                <li>Коллектор водяной на тепловоз ТЭМ-7</li>  \r\n                <li>Коллектор водяной на тепловоз ТЭМ-2</li>  \r\n                <li>Коллектор водяной на тепловоз ТГМ-3</li>  \r\n                <li>Коллектор водяной на тепловоз 2ТЭ10Л</li>  \r\n                <li>Коллектор водяной на тепловоз 2ЕЭ10М</li>  \r\n                <li>Коллектор водяной на тепловоз 2ТЭ116 и его модификации</li>  \r\n                <li>Кожух редуктора тягового агрегата электровоза ОПЭ-А</li>  \r\n                <li>Кожух редуктора тягового агрегата 2ТЭ116.30.59.050сб</li>  \r\n                <li>Кожух редуктора тягового агрегата 131.30.59.001</li>  \r\n                <li>Кожух редуктора тягового агрегата ЧМЭ</li>  \r\n                <li>Корпус редуктора черт. ЧС4; ЧС8;</li>  \r\n                <li>Валопровод 095</li>  \r\n                <li>Опорновозвращающий механизм</li>  \r\n                <li>Металлоконструкции любой сложности массой до 40 тн</li>  \r\n                <li>Хомут 2ТЭ112 30.58.011</li>  \r\n                <li>Измеритель ширины и высоты гребня колеса</li>  \r\n                <li>Шаблон для контроля профиля колеса</li>\r\n                <li>Измеритель толщины обода и разностенности ступицы</li>\r\n                <li>Шаблон на наружный диаметр колеса</li>\r\n                <li>Ремонт колесных пар электровозов ОПЭ-1А</li>\r\n                <li>Ремонт колесных пар вагонов</li>\r\n                <li>Тяга тормозная черт. 2ТЭ116.40.21.052сб</li>\r\n                <li>Выхлопной коллектор</li>\r\n                <li>Фланец 102, 105</li>\r\n                <li>Зап. части к импортной и отечественной с/х технике</li>\r\n                <li>Изготовление метизов любой сложности</li>\r\n                <li>Пескоструйные работы негабаритных деталей (а/м, тепловозы, металлоконструкций)</li>\r\n                <li>Оказание транспортных услуг или грузоперевозки</li>\r\n              </ol>', '', ''),
(7, 'З/Ч для С/Х техники', 'parts-selhoz', '', '', ' <ul>\r\n <li>лапка стельчатая;</li>\r\n <li>лемех;</li>\r\n <li>лапа культиватора КРН;</li>\r\n <li>диск БДТ;</li>\r\n <li>диск лущильника;</li>\r\n <li>лапа КПЕ;</li>\r\n</ul>', 'Запчасти и ремонт сельхозтехники <br/> любых производителей.', 'images/sh_parts2.png');

-- --------------------------------------------------------

--
-- Структура таблицы `img_carusel`
--

CREATE TABLE IF NOT EXISTS `img_carusel` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(200) NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `logo`
--

CREATE TABLE IF NOT EXISTS `logo` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(150) NOT NULL,
  `img` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `logo`
--

INSERT INTO `logo` (`id`, `title`, `img`) VALUES
(1, 'Универсалмаш', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(100) NOT NULL,
  `title_en` varchar(100) NOT NULL,
  `turn_on` int(1) NOT NULL default '1',
  `position` int(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `title`, `title_en`, `turn_on`, `position`) VALUES
(1, 'Главная', 'home', 1, 1),
(3, 'Товары и услуги', 'services', 1, 2),
(4, 'Прайсы', '#', 0, 3),
(5, 'Контакты', 'contacts', 1, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
