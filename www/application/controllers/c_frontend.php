<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_frontend extends CI_Controller {

	
	function index()
	{
		$this->load->model('m_frontend');
		$data['menu'] = $this->m_frontend->get_menu();
		$data['article_info'] = $this->m_frontend->get_article('about');
		$data['short_articles'] = $this->m_frontend->get_short_articles("parts', 'metallokonstruktsiy','parts-selhoz");
		$this->load->view('v_main',$data);
	}
	
	function myRender($var){
			$this->load->model('m_frontend');
			$data['menu'] = $this->m_frontend->get_menu(); // MENU
			
			if($this->m_frontend->get_article($var) == NULL) {
				header("Location:".base_url());
			} 
				$data['article_info'] = $this->m_frontend->get_article($var); // DATA
				
			$this->load->view('v_main',$data);
	}
	
}
