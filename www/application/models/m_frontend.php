<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_frontend extends CI_Model {

	
	public function get_menu()
	{
		$q = $this->db->query(
    	   "SELECT id,title,title_en,position 
			FROM menu 
			WHERE turn_on = 1 
			ORDER BY position ASC");

		return $q->result_array();
	}

	function get_article($article_name)
	{
		$q = $this->db->query(
			"SELECT title, title_en, text, img,meta_keywords,meta_description
			FROM articles
			WHERE title_en = '$article_name'"
			);
		if($q->num_rows > 0)
		return $q->row_array();
	}

		function get_short_articles($names)
	{
		$q = $this->db->query(
			"SELECT title, title_en, short_text, img
			FROM articles
			WHERE title_en IN ('".$names."')");
		
		return $q->result_array();
	}
}
