<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
	<meta name='yandex-verification' content='55f9ae96cde36ee0' />
    <title>Универсалмаш</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/um_style.css" rel="stylesheet">
   <!--[if (IE 7) | (IE 8)]>
    <style type="text/css">
      .background {
        background-image: url("../images/bg1_color.jpg");
        background-attachment: fixed;
        background-repeat: no-repeat;
        background-position-x: center;
        background-position-y: center;
        background-color: #999;
      }
    </style>
   <![endif]-->
</head>
<body class="background">

    <div class="container-narrow">

      <div class="masthead">
        
        <ul class="nav nav-pills pull-right top_menu" >

          <? 
          foreach($menu as $menu_item)
          {
               echo("<li class='active'><a href="."/".$menu_item['title_en'].">".$menu_item['title']."</a></li>");
          }
                     
          ?>
        </ul>

        <h3 class="logo">Универсалмаш</h3>
      </div>

      <!--<div class="jumbotron">
          
      <img src="images/gallery_2.jpg">
        
    </div> -->

    <div id="myCarousel" class="carousel slide"> 

          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
           
          </ol>
          <!-- Carousel items -->
          <div class="carousel-inner carusel">

            <div class="active item">
              <a href="http://universalmash.com.ua/parts">
                <img src="images/gallery_2.jpg">
                <div class="carousel-caption">
                  <h4>Детали и восстановительный ремонт тепловозов</h4>
                </div>
              </a> 
            </div>
            <div class="item">
              <a href="http://universalmash.com.ua/parts-selhoz">
              
                <img src="images/combain.jpg">
                <div class="carousel-caption">
                  <h4>З/Ч и восстановительный ремонт с/х техники любых производителей</h4>
              </a>    
              </div>
            </div>
            <div class="item">
              <a href="http://universalmash.com.ua/metallokonstruktsiy">
                <img src="images/mk_2.jpg">
                <div class="carousel-caption">
                  <h4>Металлоконструкции любой сложности</h4>
                </div>
              </a>
            </div>
          </div>
          <!-- Carousel nav -->
          <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
          <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>

<hr/>
<div class="content">
        <div class="row about">
          <div class="span9 article">
            <?php
            echo "<h2>".$article_info['title']."</h2>";
            echo $article_info['text'];
            ?>
          </div> 
        </div>
 
       <div class="row marketing">
        <div class="row block">
          <?php 
          if (isset($short_articles))
          {
            foreach ($short_articles as $row){ 
                   
                echo "<div class='span3 row_center'>";
                echo img(array('src' => $row['img'], 'alt' => 
                $row['title_en'])); 
                echo "<h4>".$row['title']."</h4>";

                if(strlen($row['title']) < 50) { echo "<br/>"; }

                echo  "<p>".$row['short_text']."</p><br/>";
                
                echo anchor ($row['title_en'], 'Подробнее', array('class' => 'btn btn-inverse'));   
                              
                echo "</div>";
              } 
            }
            ?>
         </div>
      </div> 
    </div>
      </hr>

      <div class="footer">
        <p class="footer_custom">ООО “ВКФ “Универсалмаш”.  Все права защищены. © 2014</p>
        <div class="emdi"></div>
      </div>

    </div> <!-- /container -->

   
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="js/bootstrap.min.js"></script>

<script type="text/javascript">

function getRandomInt(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

var bg = "bg" + getRandomInt(1,3) +"_color.jpg";
</script>


<script type="text/javascript">
  $(".background").css('background','rgba(0, 0, 0, 0) url(http://universalmash.com.ua/images/' + bg + ') no-repeat fixed 75% 100% / cover padding-box border-box');

  $('.carousel').carousel({
    interval: 10000
  })

</script> 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27421753-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--[if (IE 7) | (IE 8)]> 
  <script type="text/javascript">
    $(".background").css({
      'background-image' : 'url(http://universalmash.com.ua/images/' + bg + ')',
      'background-attachment' : 'fixed',
      'background-repeat' : 'no-repeat',
      'background-position-x' : 'center',
      'background-position-y' : 'center',
      'background-color' : '#999'
      });
  </script>
<![endif]--> 

</body>
</html>